var app = angular.module('waterReporting', ['ngRoute', 'firebase', 'uiGmapgoogle-maps', 'ui.bootstrap', 'chart.js']);

app.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.hashPrefix('!');

    $routeProvider.
        when('/', {
            templateUrl: 'partials/main.html',
            controller: 'MainController'
        }).
        when('/login', {
            templateUrl: 'partials/login.html',
            controller: 'LoginController'
        }).
        when('/register', {
            templateUrl: 'partials/register.html',
            controller: 'RegisterController'
        }).
        when('/water-report/:id', {
            templateUrl: 'partials/water-report.html',
            controller: 'ViewReportController'
        }).
        when('/add-report', {
            templateUrl: 'partials/add-report.html',
            controller: 'AddReportController'
        }).
        when('/account-information', {
            templateUrl: 'partials/account-information.html',
            controller: 'AccountInformationController'
        }).
        when('/change-password', {
            templateUrl: 'partials/change-password.html',
            controller: 'ChangePasswordController'
        }).
        when('/quality-reports', {
            templateUrl: 'partials/quality-reports.html',
            controller: 'QualityReportsController'
        }).
        when('/quality-report/:id', {
            templateUrl: 'partials/quality-report.html',
            controller: 'ViewQualityReportController'
        }).
        when('/add-quality-report', {
            templateUrl: 'partials/add-quality-report.html',
            controller: 'AddQualityReportController'
        }).
        when('/history-graph', {
            templateUrl: 'partials/history-graph.html',
            controller: 'HistoryGraphController'
        }).
        when('/report-map', {
            templateUrl: 'partials/report-map.html',
            controller: 'ReportMapController'
        }).
        otherwise({
            redirectTo: '/'
        });
}]);

app.directive('longitude', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, controller) {
            controller.$validators.longitude = function (modelValue, viewValue) {
                if (controller.$isEmpty(modelValue)) {
                    return false;
                }

                var tryParse = parseFloat(viewValue);
                if (isNaN(tryParse)) {
                    return false;
                }

                if (tryParse < -180 || tryParse > 180) {
                    return false;
                }

                return true;
            };
        }
    }
});

app.directive('latitude', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, controller) {
            controller.$validators.latitude = function (modelValue, viewValue) {
                if (controller.$isEmpty(modelValue)) {
                    return false;
                }

                var tryParse = parseFloat(viewValue);
                if (isNaN(tryParse)) {
                    return false;
                }

                if (tryParse < -90 || tryParse > 90) {
                    return false;
                }

                return true;
            };
        }
    }
});

app.directive("waterNavbar", function () {
    return {
        templateUrl: "partials/navbar.html",
        scope: {
            user: "@"
        },
        controller: 'NavbarController'
    }
});

// because I'm not including a goddamn module
var leftPad = function (str, n, c) {
    if (str.length >= n) return str;
    return leftPad(c + str, n, c);
};

var toDateFirebase = function (dateObject) {
    return {	// praise the js lords for using the same api as java's api
        date: dateObject.getDate(),
        day: dateObject.getDay(),
        hours: dateObject.getHours(),
        minutes: dateObject.getMinutes(),
        month: dateObject.getMonth(),
        seconds: dateObject.getSeconds(),
        time: dateObject.getTime(),
        timezoneOffset: dateObject.getTimezoneOffset(),
        year: dateObject.getYear()
    };
};

var toDateString = function (dateObject) {
    // cheaper than including a library
    // going for 'Y-m-d h:i:S K' from flatpickr
    return (dateObject.getYear() + 1900 + '') + '-'
        + leftPad('' + (dateObject.getMonth() + 1), 2, '0') + '-'
        + leftPad('' + dateObject.getDate(), 2, '0') + ' '
        + (dateObject.getHours() % 12 == 0 ? 12 : dateObject.getHours() % 12) + ':'
        + leftPad('' + dateObject.getMinutes(), 2, '0') + ':'
        + leftPad('' + dateObject.getSeconds(), 2, '0') + ' ' +
        (dateObject.getHours() < 12 ? 'AM' : 'PM');
};

app.directive('datetime', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attributes, controller) {
            controller.$formatters.push(function (value) {
                return toDateString(new Date(value.time));
            });

            controller.$parsers.push(function (value) {
                var dateObject = new Date(value);
                return toDateFirebase(dateObject);
            })
        }
    };
});

app.controller('NavbarController', function ($scope, $firebaseAuth, $firebaseObject, $firebaseArray, $location) {
    function init() {
        $scope.auth = $firebaseAuth();
        $scope.user = $scope.auth.$getAuth();

        var profileObject = $firebaseObject(firebase.database().ref('profile/' + $scope.user.uid));
        if (profileObject !== null)
            $scope.userProfile = profileObject;

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User') {
                $scope.showQuality = false;
                $scope.showHistory = false;
            } else if (profileObject.role === 'Worker') {
                $scope.showQuality = true;
                $scope.showHistory = false;
            } else {
                $scope.showQuality = true;
                $scope.showHistory = true;
            }
        });
    }

    init();

    $scope.logout = function () {
        $scope.auth.$signOut();
    }
});

app.controller('LoginController', function ($scope, $firebaseAuth, $location) {
    $scope.auth = $firebaseAuth();

    $scope.email = 'ng-test@example.com';
    $scope.password = 'abc123';
    $scope.error = '';

    $scope.alerts = [];

    $scope.login = function () {
        if ($scope.email === undefined) {
            $scope.error = 'Email is invalid';
            return;
        }

        $scope.auth.$signInWithEmailAndPassword($scope.email, $scope.password).
            then(function (firebaseUser) {
                $location.path('/').replace();
            }).
            catch(function (error) {
                $scope.error = error;
                console.log(error);
                $scope.createAlert(error.message, "danger")
            });
    }

    $scope.createAlert = function (message, type) {
        $scope.alerts.push({ msg: message, type: type });
    }

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    }
});

app.controller('RegisterController', function ($scope, $firebaseAuth, $firebaseObject, $location) {
    $scope.auth = $firebaseAuth();
    $scope.error = '';
    $scope.role = 'User';
    $scope.alerts = [];

    $scope.register = function () {
        if ($scope.email === undefined) {
            $scope.error = 'Email is invalid';
            return;
        }

        console.log("hi");

        if ($scope.password !== $scope.passwordConfirm) {
            $scope.error = 'Passwords do not match';
            // alert("Passwords do not match!");/
            $scope.createAlert("Your passwords don't match!", "danger");
            return;
        }

        $scope.auth.$createUserWithEmailAndPassword($scope.email, $scope.password).
            then(function (firebaseUser) {
                var uid = firebaseUser.uid;
                var dbReference = firebase.database().ref('profile/' + uid);

                var profile = {
                    name: $scope.name,
                    role: $scope.role
                };

                dbReference.set(profile).
                    then(function () {
                        $location.path('/').replace();
                        $scope.$apply();
                    }).
                    catch(function (error) {
                        $scope.error = error;		// this is really bad. data inconsistencies.
                        $scope.createAlert("Your profile could not be created!", "danger");
                        $scope.$apply();
                    });
            }).
            catch(function (error) {
                $scope.error = error;
                console.log($scope.error);
                $scope.createAlert(error.message, "danger");
            });
    }

    $scope.createAlert = function (message, type) {
        $scope.alerts.push({ msg: message, type: type });
    }

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    }
});

app.controller('MainController', function ($scope, $firebaseAuth, $firebaseObject, $firebaseArray, $location) {
    $scope.auth = $firebaseAuth();
    $scope.firebaseUser = $scope.auth.$getAuth();

    if ($scope.firebaseUser === null) {
        $location.path('/login').replace();
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login').replace();
            return;
        }

        checkCredentials();
    });

    var checkCredentials = function () {
        var user = $scope.auth.$getAuth();
        var uid = user.uid;
        var profileObject = $firebaseObject(firebase.database().ref('profile/' + uid));

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User') {
                $scope.showQuality = false;
                $scope.showHistory = false;
            } else if (profileObject.role === 'Worker') {
                $scope.showQuality = true;
                $scope.showHistory = false;
            } else {
                $scope.showQuality = true;
                $scope.showHistory = true;
            }
        });
    }

    checkCredentials();

    $scope.logout = function () {
        $scope.auth.$signOut();
    }

    var reportsReference = firebase.database().ref('source_report');
    var reportsArray = $firebaseArray(reportsReference);

    $scope.reportData = reportsArray;
});

app.controller('ViewReportController', function ($scope, $firebaseAuth, $firebaseObject, $location, $routeParams) {
    $scope.auth = $firebaseAuth();

    $scope.conditionString = {
        'WASTE': 'Waste',
        'TREATABLE_CLEAR': 'Treatable (clear)',
        'TREATABLE_MUDDY': 'Treatable (muddy)',
        'POTABLE': 'Potable'
    };

    $scope.typeString = {
        'BOTTLED': 'Bottled',
        'WELL': 'Well',
        'STREAM': 'Stream',
        'LAKE': 'Lake',
        'SPRING': 'Spring',
        'OTHER': 'Other'
    }

    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            return;
            $location.path('/login');
        }
    });

    $scope.renderDatetime = function (datetime) {
        return (new Date(datetime)).toString();
    };

    var reportReference = firebase.database().ref('source_report/' + $routeParams.id);
    var reportsObject = $firebaseObject(reportReference);

    $scope.report = reportsObject;
});

app.controller('AddReportController', function ($scope, $firebaseAuth, $firebaseArray, $firebaseObject, $location, $routeParams, $timeout) {
    $scope.auth = $firebaseAuth();

    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            return;
            $location.path('/login');
        }
    });

    $scope.report = {
        title: 'New water report',
        location: {
            longitude: 0,
            latitude: 0
        },
        datetime: toDateFirebase(new Date()),
        waterCondition: 'POTABLE',
        waterType: 'WELL'
    };

    $scope.addReport = function () {
        var reportsArray = $firebaseArray(firebase.database().ref('source_report'));
        reportsArray.$add($scope.report).then(function (reference) {
            var reportObject = $firebaseObject(reference);
            reference.child('reportNumber').set(reference.key);
            reportObject.$bindTo($scope, 'report');
        });

        $location.path('/');
    }

    // after the view is rendered, enable the calendar plugin
    $timeout(function () {
        flatpickr('.flatpickr', {
            dateFormat: 'Y-m-d h:i:S K',
            enableTime: true
        });
    }, 0);
});

app.controller('HistoryGraphController', function ($scope, $firebaseAuth, $firebaseObject, $firebaseArray, $location, $routeParams) {
    $scope.auth = $firebaseAuth();
    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }

        checkCredentials();
    });

    var checkCredentials = function () {
        var user = $scope.auth.$getAuth();
        var uid = user.uid;

        var profileObject = $firebaseObject(firebase.database().ref('profile/' + uid));

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User') {
                $location.path('/');
            }
        });
    }

    checkCredentials();

    var reportsReference = firebase.database().ref('quality_report');
    $scope.reports = $firebaseArray(reportsReference);

    console.log('Reports', $scope.reports);
    $scope.showReports = { x: [], y: [] };
    $scope.searchInfo = {
        latitude: 0,
        longitude: 0,
        contaminant: true,
        year: 2017
    };

    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];


    $scope.searchReports = function () {
        $scope.showReports = { x: [], y: [] };

        for (let report of $scope.reports) {
            let date = new Date(report.datetime.time);
            if (
                (date.getFullYear() === $scope.searchInfo.year)
                && (report.location.latitude === $scope.searchInfo.latitude)
                && (report.location.longitude === $scope.searchInfo.longitude)
            ) {
                console.log(report);
                $scope.showReports.x.push(monthNames[date.getMonth()]);
                if ($scope.searchInfo.contaminant) {
                    $scope.showReports.y.push(report.contaminantPPM);
                } else {
                    $scope.showReports.y.push(report.virusPPM);
                }
            }
        }
        console.log("SHOW", $scope.showReports);
    };

    $scope.reports.$loaded().then(function () {
        $scope.searchReports();
    });
});

app.controller('QualityReportsController', function ($scope, $firebaseAuth, $firebaseObject, $firebaseArray, $location, $routeParams) {
    $scope.auth = $firebaseAuth();
    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }

        checkCredentials();
    });

    var checkCredentials = function () {
        var user = $scope.auth.$getAuth();
        var uid = user.uid;

        var profileObject = $firebaseObject(firebase.database().ref('profile/' + uid));

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User' || profileObject.role === 'Worker') {
                $location.path('/');
            }
        });
    }

    checkCredentials();

    var reportsReference = firebase.database().ref('quality_report');
    var reportsArray = $firebaseArray(reportsReference);

    $scope.reportData = reportsArray;
});

app.controller('AddQualityReportController', function ($scope, $firebaseAuth, $firebaseArray, $firebaseObject, $location, $routeParams, $timeout) {
    $scope.auth = $firebaseAuth();
    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }

        checkCredentials();
    });

    var checkCredentials = function () {
        var user = $scope.auth.$getAuth();
        var uid = user.uid;

        var profileObject = $firebaseObject(firebase.database().ref('profile/' + uid));

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User') {
                $location.path('/');
            }
        });
    }

    checkCredentials();

    $scope.report = {
        title: 'New quality report',
        location: {
            longitude: 0,
            latitude: 0
        },
        datetime: toDateFirebase(new Date()),
        virusPPM: 0,
        contaminantPPM: 0,
        waterCondition: 'POTABLE'
    };

    $scope.addQualityReport = function () {
        var reportsArray = $firebaseArray(firebase.database().ref('quality_report'));
        reportsArray.$add($scope.report).then(
            function (reference) {
                var reportObject = $firebaseObject(reference);
                reference.child('reportNumber').set(reference.key);
                reportObject.$bindTo($scope, 'report');
            },
            function (error) {
                console.log(error.message);
            }
        );

        $location.path('/quality-reports');
    }

    // after the view is rendered, enable the calendar plugin
    $timeout(function () {
        flatpickr('.flatpickr', {
            dateFormat: 'Y-m-d h:i:S K',
            enableTime: true
        });
    }, 0);
});

app.controller('ViewQualityReportController', function ($scope, $firebaseAuth, $firebaseObject, $location, $routeParams) {
    $scope.auth = $firebaseAuth();
    $scope.conditionString = {
        'WASTE': 'Waste',
        'TREATABLE_CLEAR': 'Treatable (clear)',
        'TREATABLE_MUDDY': 'Treatable (muddy)',
        'POTABLE': 'Potable'
    };

    var firebaseUser = $scope.auth.$getAuth();

    if (firebaseUser === null) {
        $location.path('/login');
        return;
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }

        checkCredentials();
    });

    var checkCredentials = function () {
        var user = $scope.auth.$getAuth();
        var uid = user.uid;

        var profileObject = $firebaseObject(firebase.database().ref('profile/' + uid));

        profileObject.$loaded().then(function () {
            if (profileObject.role === 'User') {
                $location.path('/');
            }
        });
    }

    $scope.renderDatetime = function (datetime) {
        return (new Date(datetime)).toString();
    };
    var reportReference = firebase.database().ref('quality_report/' + $routeParams.id);
    var reportsObject = $firebaseObject(reportReference);
    $scope.report = reportsObject;
});

app.controller('AccountInformationController', function ($scope, $firebaseAuth, $firebaseObject, $location) {
    $scope.auth = $firebaseAuth();

    $scope.firebaseUser = $scope.auth.$getAuth();
    $scope.alerts = [];

    if ($scope.firebaseUser === null) {
        $location.path('/login');
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }
    });

    var profileReference = firebase.database().ref('profile/' + $scope.firebaseUser.uid);
    var profileObject = $firebaseObject(profileReference);

    profileObject.$bindTo($scope, 'profile');

    $scope.createAlert = function (message, type) {
        $scope.alerts.push({ msg: message, type: type });
    }

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    }

    $scope.updateProfile = function () {
        $scope.createAlert("Account Updated Successfully!", "success");
    }
});

app.controller('ChangePasswordController', function ($scope, $firebaseAuth, $firebaseObject, $location) {
    $scope.auth = $firebaseAuth();

    $scope.firebaseUser = $scope.auth.$getAuth();
    $scope.alerts = [];

    if ($scope.firebaseUser === null) {
        $location.path('/login');
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }
    });

    $scope.changePassword = function () {
        if ($scope.changePasswordField === null) {
            $scope.createAlert("Password cannot be empty!", "danger");
            return;

        }
        if ($scope.changePasswordField !== $scope.changePasswordConfirm) {
            $scope.error = 'Passwords do not match';
            $scope.createAlert("Passwords do not match!", "danger");
            return;
        }

        $scope.auth.$updatePassword($scope.changePasswordField).then(function () {
            $location.path('/account-information');
        }).catch(function (error) {
            $scope.error = error;
            $scope.createAlert(error.message, "danger");
        });
    };

    $scope.createAlert = function (message, type) {
        $scope.alerts.push({ msg: message, type: type });
    }

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    }
});

app.controller('ReportMapController', function ($scope, $firebaseAuth, $firebaseArray, $firebaseObject, $location) {
    $scope.auth = $firebaseAuth();

    $scope.firebaseUser = $scope.auth.$getAuth();

    $scope.map = { center: { latitude: 0, longitude: 0 }, zoom: 2 };

    if ($scope.firebaseUser === null) {
        $location.path('/login');
    }

    $scope.auth.$onAuthStateChanged(function (firebaseUser) {
        if (firebaseUser === null) {
            $location.path('/login');
            return;
        }
    });

    $scope.showWaterReport = function (reportNumber) {
        $location.path('/water-report/' + reportNumber);
        $scope.$apply();	// for some reason, without this, it won't go until you drag the map
    }

    var reportsReference = firebase.database().ref('source_report');
    var reportsArray = $firebaseArray(reportsReference);

    $scope.reports = reportsArray;
});